﻿
'use strict';

//A model should have the fpollowing format. and evert controller
//that is bind to it will have to have myApp.controller(........);
var myApp = angular.module('myApp', []);

myApp.config(['$routeProvider', function ($routeProvider) {


    $routeProvider.when("/PhoneList", {
        controller: "phoneListCtrl",
        templateUrl: "/Angular/Templates/PhoneListView.html"
    });
    
    $routeProvider.when("/myCalculator", {
        controller: "myCalculator",
        templateUrl: "/Angular/Templates/CalculatorView.html"
    });
    
    $routeProvider.when("/myMessages", {
        controller: "myMessagesCtrl",
        templateUrl: "/Angular/Templates/MessagesView.html"
    });
    
    $routeProvider.when("/Test one", {
        controller: "myMessagesCtrl",
        templateUrl: "/Angular/Templates/TestOneView.html"
    });

    $routeProvider.otherwise({ redirectTo: "/" });
}]);