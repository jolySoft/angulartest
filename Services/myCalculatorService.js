﻿/// <reference path="myCalculatorService.js" />
'use strict';

//This is the calculator service that responds to the calculator controller

myApp.factory('myCalculatorServices', function ($http) {

    var getResult = function (values) {
        return $http.post('api/calculator', values);
    };

    return {
        getResult: getResult
    };

});