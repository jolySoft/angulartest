﻿'use strict';

myApp.factory("messageService", function($http) {
    var _getMessage = function(msg) {
        return $http.post("/api/Messages", msg);
    };
    
    return {
        getMessage: _getMessage
    };
});