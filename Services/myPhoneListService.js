﻿'use strict';

//This is a data service that returns a list of phones to the calling controller
myApp.factory('myPhoneListService', function ($http, $q) {

    /**********************Private parts of the Servives************************************************/
    var phones = [];
    var getphones = function() {

        var deferred = $q.defer();

        $http.get("/api/Phone")
            .then(function(result) {
                //Sucess
                angular.copy(result.data, phones);
             
                deferred.resolve();

            }, function() {
                //Error
                deferred.reject();
            });

        return deferred.promise; //This will be returning to the controller
    };
    
    /**********************public parts of the Servives************************************************/
    return {
        phoneList: phones,
        getPhones: getphones
    };
    
});
