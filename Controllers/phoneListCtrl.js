﻿
'use strict';

//A controller to be bound to a model, the the following format shoud be followed.
//Pay more attention to the first line and the last brancket
//myPhoneList is the Service that supplies the list of phones.

myApp.controller('phoneListCtrl', function phoneListCtrl($scope, myPhoneListService) {
    $scope.phones = myPhoneListService; //this means $scope.phones will have phoneList. Hence the View should access phones.phoneList, which contains the array

    myPhoneListService.getPhones()
        .then(function() {
            //Seccess
        }, function () {
            //Eror

            alert("What you have you fed me with??");

        });

    $scope.orderProp = 'age';
});

//phoneListController.$inject = ['$scope'];