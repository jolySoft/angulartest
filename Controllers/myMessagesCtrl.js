﻿'use string';

//A controller to be bound to a model, the the following format shoud be followed.
//Pay more attention to the first line and the last brancket
//To pass parameter to the service, use the property returned.


myApp.controller('myMessagesCtrl', function myMessagesCtrl($scope, messageService) {

    $scope.message = "hello mohamed!";
    
    $scope.greeting = function () {

        var value = { message: $scope.message };
    
        messageService.getMessage(value)
            .then(function (response) {
                //Success
                $scope.message = response.data.message;
            }, function () {
                alert("Are you kidding me");
            });
    };

});