﻿'use strict';

//A controller to be bound to a model, the the following format shoud be followed.
//Pay more attention to the first line and the last brancket

myApp.controller('myCalculator', function myCalculator($scope, myCalculatorServices) {

    $scope.result = 0;


    $scope.calculateValues = function(myaction) {

        var values = { a: $scope.a, b: $scope.b, action: myaction };
        myCalculatorServices.getResult(values)
            .then(function (response) {
                //Success
                $scope.result = response.data;
            }, function() {
                //error
                alert("are you kidding me!");
            });
    };

    $scope.work = ": Is this working";
});