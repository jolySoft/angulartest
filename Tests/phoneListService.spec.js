﻿/*global describe, beforeEach, afterEach, module, inject, it, expect, spyOn, unused, angular */

describe('Phone List Service', function () {
    'use strict';

    var q;

    beforeEach(function () {
        module('myApp');
    });

    beforeEach(inject(function ($rootScope, $controller, $injector, $q, $httpBackend) {
        //console.log('BeforeRun');
        q = $q;

        this.$rootScope = $rootScope;
        this.$httpBackend = $httpBackend;
        this.phoneService = $injector.get('myPhoneListService');
    }));

    describe('Get Phone List', function () {
        it('adds list to phoneList', function() {
            this.$httpBackend.expectGET('/api/Phone').respond(200, [123, 123, 123, 123] );

            this.phoneService.getPhones();

            this.$httpBackend.flush();

            expect(this.phoneService.phoneList.length).toBe(4);
            expect(this.phoneService.phoneList[0]).toBe(123);
            expect(this.phoneService.phoneList[1]).toBe(123);
            expect(this.phoneService.phoneList[2]).toBe(123);
            expect(this.phoneService.phoneList[3]).toBe(123);
        });

        it('list is empty on error', function() {
            this.$httpBackend.expectGET('/api/Phone').respond(500, '');

            this.phoneService.getPhones();

            this.$httpBackend.flush();

            expect(this.phoneService.phoneList.length).toBe(0);
        });
    });
});