﻿/*global describe, beforeEach, afterEach, module, inject, it, expect, spyOn, unused, angular */

describe('My Calculator Controller', function() {
    'use strict';

    var q;

    beforeEach(function() {
        module('myApp');
    });

    beforeEach(inject(function ($rootScope, $controller, $injector, $q, $httpBackend) {
        //console.log('BeforeRun');
        q = $q;

        this.$rootScope = $rootScope;
        this.$scope = $rootScope.$new();
        this.$controller = $controller;
        this.$httpBackend = $httpBackend;
        this.calculatorService = $injector.get('myCalculatorServices');

        this.calculatorController = $controller('myCalculator', {
            $scope: this.$scope,
            myCalculatorServices: this.calculatorService
        });
    }));
    
    describe('when calculating values', function () {
        it('it pulls data from $scope', function () {
            var valueToCheck;
            this.$scope.a = 'a';
            this.$scope.b = 'b';
            spyOn(this.calculatorService, 'getResult').andCallFake(function (values) {
                valueToCheck = values;
                var deferred = q.defer();
                deferred.resolve({ data: 'x' });
                return deferred.promise;
            });

            this.$scope.calculateValues('action');
            this.$rootScope.$digest();
            
            expect(valueToCheck.a).toBe('a');
            expect(valueToCheck.b).toBe('b');
        });

        it('it sets result on success', function () {
            var valueToCheck;
            this.$scope.a = 'a';
            this.$scope.b = 'b';
            spyOn(this.calculatorService, 'getResult').andCallFake(function (values) {
                valueToCheck = values;
                var deferred = q.defer();
                deferred.resolve({ data: 'x' });
                //console.log(values);
                return deferred.promise;
            });

            
            this.$scope.calculateValues('action');
            this.$rootScope.$digest();

            expect(this.calculatorService.getResult).toHaveBeenCalled();
            expect(this.$scope.result).toBe('x');
        });

        it('it alerts the user on fail', function () {
            var valueToCheck;
            this.$scope.a = 'a';
            this.$scope.b = 'b';
            spyOn(window, 'alert');
            spyOn(this.calculatorService, 'getResult').andCallFake(function (values) {
                valueToCheck = values;
                var deferred = q.defer();
                deferred.reject();
                return deferred.promise;
            });


            this.$scope.calculateValues('action');
            this.$rootScope.$digest();

            expect(window.alert).toHaveBeenCalled();
        });
    });
});