﻿module.exports = function(config) {
    "use strict";
    config.set({
        basePath: '..',
        files: [
            'Scripts/angular.min.js',
            'Scripts/q.min.js',
            'Scripts/angular-mocks.js',
            'apps/**/*.js',
            'Controllers/**/*.js',
            'Services/**/*.js',
            
            'Tests/myCalculator.spec.js',
            'Tests/myMessage.spec.js',
            'Tests/phoneListCtrl.spec.js',
            'Tests/phoneListService.spec.js'
        ],
        //reporters: ['progress', 'junit', 'coverage'],
        //coverageReporter: {
        //    type: 'html',
        //    dir: 'Tests/Coverage'
        //},
        //junitReporter: {
        //    outputFile: 'Test-results.xml',
        //    suite: ''
        //},
        frameworks: ["jasmine"],
        browsers: ['PhantomJS'],
        singleRun: false,
        autoWatch: true,
        autoWatchInterval: 500
    });
};