﻿/*global describe, beforeEach, afterEach, module, inject, it, expect, spyOn, unused, angular */

describe('My Message Controller', function() {
    'use strict';

    var q;

    beforeEach(function () {
        module('myApp');
    });

    beforeEach(inject(function ($rootScope, $controller, $injector, $q, $httpBackend) {
        //console.log('BeforeRun');
        q = $q;

        this.$rootScope = $rootScope;
        this.$scope = $rootScope.$new();
        this.$controller = $controller;
        this.$httpBackend = $httpBackend;
        this.messageService = $injector.get('messageService');

        this.calculatorController = $controller('myMessagesCtrl', {
            $scope: this.$scope,
            messageService: this.messageService
        });
    }));

    describe('when posting a message', function() {
        it('it pulls data from $scope', function() {
            var valueToCheck;
            this.$scope.message = 'Hello World!';
            spyOn(this.messageService, 'getMessage').andCallFake(function(values) {
                valueToCheck = values;
                var deferred = q.defer();
                deferred.resolve({ data: { message: 'Hello Jolyon' } });
                return deferred.promise;
            });

            
            this.$scope.greeting();
            this.$rootScope.$digest();

            expect(valueToCheck.message).toBe('Hello World!');
        });

        it('set message on success', function() {
            var valueToCheck;
            this.$scope.message = 'Hello World!';
            spyOn(this.messageService, 'getMessage').andCallFake(function (values) {
                valueToCheck = values;
                var deferred = q.defer();
                deferred.resolve({ data: { message: 'Hello Jolyon' } });
                return deferred.promise;
            });


            this.$scope.greeting();
            this.$rootScope.$digest();

            expect(this.$scope.message).toBe('Hello Jolyon');
        });

        it('alert the user on fail', function() {
            var valueToCheck;
            this.$scope.message = 'Hello World!';
            spyOn(window, 'alert');
            spyOn(this.messageService, 'getMessage').andCallFake(function (values) {
                valueToCheck = values;
                var deferred = q.defer();
                deferred.reject();
                return deferred.promise;
            });


            this.$scope.greeting();
            this.$rootScope.$digest();

            expect(window.alert).toHaveBeenCalled();
        });
    });
})