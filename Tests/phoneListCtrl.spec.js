﻿/*global describe, beforeEach, afterEach, module, inject, it, expect, spyOn, unused, angular */

describe('My Phone List Controller', function() {
    'use strict';

    var q;

    beforeEach(function () {
        module('myApp');
    });

    beforeEach(inject(function ($rootScope, $controller, $injector, $q, $httpBackend) {
        //console.log('BeforeRun');
        q = $q;

        this.$rootScope = $rootScope;
        this.$scope = $rootScope.$new();
        this.$controller = $controller;
        this.$httpBackend = $httpBackend;
        this.phoneService = $injector.get('myPhoneListService');

        this.serviceData = { data: [123, 123, 123, 123] };

        spyOn(this.phoneService, 'getPhones').andCallFake(function () {
            var deferred = q.defer();
            deferred.resolve(this.serviceData);
            //console.log(values);
            return deferred.promise;
        });
        
        this.phoneController = $controller('phoneListCtrl', {
            $scope: this.$scope,
            phoneService: this.phoneService
        });
    }));

    it('calls the service in the constructor', function() {
        this.$rootScope.$digest();

        expect(this.phoneService.getPhones).toHaveBeenCalled();
    });

})